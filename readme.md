## Web Dev Recruit - Backend Question

### How to run.
1. Clone repo to local
`
 git clone https://gitlab.com/Suthiphong/backend_devcrew
`
2. install package
`
 npm install or yarn install
`
3. run
`node server.js or npm start`

****
## Software POSTMAN

API | Method | Send | Authorization |Parameter | Result
--- | --- | --- | --- | --- | ---
/cale | POST | Body->x-www-form-urlcoded |DEVCREW-BACKEND-TEST| {a:float, b: float} | {result: a*b}
/upload | POST | Body->form-data |-| select-file | {fileName: name of file, size: size in MB, extension: extension} 