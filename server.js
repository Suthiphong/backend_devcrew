const 
    express = require('express')
    fileUpload = require('express-fileupload')
    bodyParser = require('body-parser')
    cors = require('cors')
    Run = express()


Run.use(cors());
Run.use(bodyParser.json());
Run.use(bodyParser.urlencoded({extended: true}));
Run.use(fileUpload())

const STATIC_TEXT = {
    AUTHOR: "DEVCREW-BACKEND-TEST",
    UNAUTHORIZED: "Unauthorized",
    FORMAT: "Unsupported data format",
    FileNull: "Please select a file"
} 

Run.post('/cale', (req,res) => {
    const {a,b} = req.body
    const Author = req.header("Authorization")
    //console.log(Author)
    if(Author == STATIC_TEXT.AUTHOR) {
        const result = Calc(a,b)
        if(result.error != null) {
            res.status(result.Status).json({error: result.error})
        }else {
            res.status(result.Status).json({result: result.result})
        }
    }else {
        res.status(401).json({error: STATIC_TEXT.UNAUTHORIZED})
    }
})

Run.post('/upload', async (req,res)=> {
    try {
        //console.log(req.files)
        if(!req.files){ 
            res.status(422).json({error: STATIC_TEXT.FileNull})
        }else {
            let file = req.files.file
            res.status(200).json({
                fileName: file.name,
                size: formatBytes(file.size),
                extension: file.name.split(".")[1]
            })
        }
    } catch (err) {

    }
})


Run.get('/', (req,res)=> {
    console.log('/') 
    res.send('hello')
})


Run.listen(8888, () => console.log('server running port 8888'))


function Calc(a=NaN,b=NaN) {
    let A = checkVal(a)
    let B = checkVal(b) 
    if(A && B) {
        return {
            Status: 200,
            error: null,
            result: A*B
        }
    }else {
        return {
                Status: 422,
                result:null,
                error: "Unsupported data format"
            }
    }
}

function formatBytes(byte) {
    let MB = byte * 0.000001
    return `${MB}MB`
}

function checkVal(val = NaN) {
    if(val == NaN) {
        return NaN
    }else {
        if(!isNaN(parseFloat(val))) {
            return parseFloat(val)
        }
        else {
            return NaN
        }
    }
}